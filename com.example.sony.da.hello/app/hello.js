//============================
// TODO:
//   背景のBGM削除
//   途中で入る音声削除
//　　著作権的に曲を入れ替える必要あり
//============================

da.segment.onpreprocess = function (trigger, args) {
    console.log("===y=== : 1-1");
    console.log('onpreprocess', { trigger: trigger, args: args });
    console.log("===y=== : 1-2");
    da.startSegment(null, null);
    console.log("===y=== : 1-3");
};

da.segment.onstart = function (trigger, args) {

    console.log("===y=== : 3-1");

    // ====== 曲のロード ====== //
    //var audio = new Audio('http://www.freesound.org/data/previews/59/59569_571436-lq.mp3'); // 馬の鳴き声
    var audio1 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/9eb563ec0ff2df4f27db5ee1e1479c5924c0b49a/soundA/state_normal.mp3');
    var audio2 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/9eb563ec0ff2df4f27db5ee1e1479c5924c0b49a/soundA/state_denger.mp3');
    var audio3 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/9eb563ec0ff2df4f27db5ee1e1479c5924c0b49a/soundA/state_area.mp3');

    // ====== 各種変数初期化 ====== //
    var loop = 0;
    var state = 1; //[1=通常],[2=ゾンビ近い],[3=長時間いるとゾンビ化]
    audio1.play(); //とりあえず1曲目流し始める

    var hoge = setInterval(function() {

        // 「setInterval」は、周期的に処理を行うための関数である。
        // この際、前の処理が終わろうが終わらなかろうが次の処理を始めるため、
        // 一連の処理にかかる最大時間に周期を合わせる

        // GPS処理、firebase処理等
        // ここに挿入する

        console.log("===y=== : 音楽部分スタートloop_count=",loop,"_state=",state);

        // ====== デバッグ用 : ここから ====== //
        if (loop<15)
            state=1;
        else if (loop<30)
            state=2;
        else if (loop<45)
            state=3;
        else
            state=1;
        // ====== デバッグ用 : ここまで ====== //

        // ====== stateによって再生する曲を選択 ====== //
        if (state==1){
            audio1.play();
            audio2.pause();
            audio3.pause();
        }
        else if (state==2){
            audio1.pause();
            audio2.play();
            audio3.pause();
        }
        else{
            audio1.pause();
            audio2.pause();
            audio3.play();
        }

        loop++;
        // 終了条件
        if (loop == 100) {
        clearInterval(hoge);
        da.stopSegment();
        console.log("終わり");
        }
    }, 1000);

};